from flask import jsonify
from app import create_app

app = create_app()


@app.route('/health_check')
def test():
    from app import db

    result = db.session.execute('SELECT 1')
    data = [row[0] for row in result]

    return jsonify(data)
