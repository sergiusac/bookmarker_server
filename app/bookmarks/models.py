from app import db
from app.common.mixins import DateAuditMixin


bookmark_to_tag = db.Table(
    'bookmark_to_tag',
    db.Column('bookmark_id', db.Integer, db.ForeignKey('bookmark.id', ondelete='CASCADE'), primary_key=True),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id', ondelete='CASCADE'), primary_key=True)
)


class Bookmark(DateAuditMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bookmark_name = db.Column(db.String(255), nullable=False, index=True)
    description = db.Column(db.Text)
    link = db.Column(db.String)
    folder_id = db.Column(db.Integer, db.ForeignKey('folder.id', ondelete='CASCADE'))

    tags = db.relationship(
        'Tag',
        secondary=bookmark_to_tag,
        lazy='subquery',
        backref=db.backref('bookmarks', lazy=True)
    )


class Folder(DateAuditMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    folder_name = db.Column(db.String, nullable=False, index=True)
    parent_folder_id = db.Column(db.Integer, db.ForeignKey('folder.id', ondelete='CASCADE'))

    bookmarks = db.relationship('Bookmark', backref='folder', lazy=True)
    child_folders = db.relationship('Folder', backref=db.backref('parent_folder', remote_side=[id]))


class Tag(DateAuditMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag_name = db.Column(db.String(128), nullable=False, index=True)
