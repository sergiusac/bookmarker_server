from . import bp


@bp.route('/', methods=['GET'])
def index():
    return {
        'msg': 'bookmarks_index'
    }
