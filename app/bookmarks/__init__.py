from flask import Blueprint

bp = Blueprint('bookmarks', __name__)

from .routes import *
from .models import *
