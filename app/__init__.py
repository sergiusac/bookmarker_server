from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import BaseConfig

db = SQLAlchemy()
migrate = Migrate()


def create_app(config=BaseConfig):
    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    migrate.init_app(app, db)

    from .bookmarks import bp as bookmarks_bp
    app.register_blueprint(bookmarks_bp, url_prefix='/bookmarks')

    return app
