import os


class BaseConfig:
    SECRET_KEY = os.environ.get('SECRET_KEY', 'dev_secret_key')
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URI',
        'postgresql+psycopg2://{user}:{password}@{host}:{port}/{dbname}'.format(
            user='postgres',
            password='postgres',
            host='192.168.99.102',
            port='15432',
            dbname='bookmarker_db_dev'
        )
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
